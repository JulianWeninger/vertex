Welcome to the Utopia Models documentation!
===========================================

This documentation only covers the models included in this very repository.
For the documentation of Utopia, its basic models, and further guides,
please refer to the online documentation or move to the main Utopia repository
to build it locally yourself!

.. toctree::
   :maxdepth: 1
   :glob:

   The README <README>
   C++ Documentation <cpp_doc>

----

.. toctree::
   :caption: Models
   :maxdepth: 1
   :glob:

   models/*
   
----

.. note::

  If you notice any errors in this documentation, even minor, or have suggestions for improvements, please inform `Julian <julian.weninger@unige.ch>`_. Thank you! :)
