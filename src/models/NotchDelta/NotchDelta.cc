#include <iostream>

#include "NotchDelta.hh"
#include "NotchDelta_write_tasks.hh"
#include "Differentiation_write_tasks.hh"

using namespace Utopia::Models::NotchDelta;
using namespace Utopia::Models::Differentiation::DataIO;


int main (int, char** argv) {
    try {
        // Initialize the PseudoParent from a config file path
        Utopia::PseudoParent pp(argv[1]);

        // Initialize the main model instance and directly run it
        NotchDelta("NotchDelta", pp, {},
            std::make_tuple(density_time,  density_progenitor, density_hair,
                density_support, density_rosettes,
                CM_time, cell_type, cluster_id, DataIO::cell_atoh1)
        ).run();

        // Done.
        return 0;
    }
    catch (Utopia::Exception& e) {
        return Utopia::handle_exception(e);
    }
    catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    catch (...) {
        std::cerr << "Exception occurred!" << std::endl;
        return 1;
    }
}
