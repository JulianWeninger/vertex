# The model configuration for the PCPTopology model
#
# This file should ONLY contain model-specific configuration and needs to be
# written such that it can be used by _every_ model instance, regardless
# of the level within a model hierarchy.
# 
# To that end, it needs to specify the parameters at the level of the instance
# of a Model class.
# 
# This file is usually handled by the frontend: usage of advanced yaml features
# like anchors and tags is supported.
# 
# Note, however, that this file should hold the _default_ values for a single
# instance. Therefore, parameter sweeps should not be specified here!
# Furthermore, if including other models' parameters via the `!model` tag, make
# sure that no circular includes occur.
---
# --- Space -------------------------------------------------------------------
# The physical space this model is embedded in
space: &SPACE
  extent: [1., 1.]  # NOTE this is ignored in vertex model
  periodic: true

# --- Initialisation ----------------------------------------------------------
# Initialisation refers to operations that are called prior to the normal 
# operations to generate an initial state
# 0. Add noise to the system
# 1. Proliferate sufficient number of cells by generations of cell division

initialisation:
  # Add some noise to initial lattice
  0_add_noise:
    enabled: false
    minimization: 
      num_repeat: 1

  # Proliferate cells
  1_by_proliferation:
    enabled: true
    num_generations: 2
    # put here how many generations should be proliferated
    # every generation doubles in number of cells
    minimization_between_generations:
      num_repeat: 6
    minimization: ~

# --- Operations --------------------------------------------------------------
# Define here what operations shall be called when and how the vertex model is
# minimized after

# The sequence of operations, called in order
operations:
  - void: # this is a void operation, but calling minimization
      times: 
        start: 1
        stop: 100000
        step: 1


# --- Dynamics
# Minimization parameter
minimization: &Minimization
  # Numerical step size
  dt: 0.05        

  # Number of steps per minimization
  num_steps: 20

  # When using linetension fluctuations
  linetension_fluctuations: &dL 0.001
  linetension_fluctuations_tau: &tau_dL 24

  # When using jiggle vertices
  jiggle_intensity: 0.
  jiggle_tolerance: 1.e-6
  tolerance: 1.e-8
  max_steps: 15000

  # How often to repeat minimization
  num_repeat: *tau_dL
  # With dL: How many tau to integrate
  # With jiggle: How often to jiggle


# --- Vertex-model parameter --------------------------------------------------
# The model for energy minimization
PCPVertex:
  # Details of logger
  log_level: info
  # available
  #   - error     Print only error
  #   - warn      Print also warnings
  #   - info      Print more information (progress, ...)
  #   - debug     Print information important for debugging
  #   - trace     Print everything

  space: *SPACE


  # --- The work-function ---
  work_function_terms:
  - area_elasticity:
      elastic_modulus: 1
      preferential_area: 1

  - cell_contractility:
      contractility: 1.
      preferential_shape: 3.72

  - linetension_fluctuations:
      amplitude: *dL
      timescale: *tau_dL


  # --- Initialization parameters ---
  initial_jiggle: 1.e-4
  agent_manager:
    setup_method: hexagonal
    
    setup_params:
      hexagonal:
        hexagon_size: !expr (2 * 1 / 3**1.5)**0.5
        hexagon_shape: pointy_top
        lattice_rows: 4
        lattice_columns: 4
    
    vertex_manager: ~

    edge_manager: ~

    cell_manager:
      agent_params:
        type: 0

  minimization: *Minimization
  Energy_buffer_size: !expr 24 * 20


  # --- Transition parameters ---
  T1_threshold: 0.03
  T1_separation_factor: 1.05
  T1_probability: 1.
  T1_barrier: 0.06
  T1_timeout: 12
  T2_threshold: 0.0075


  # --- Data writing on PCPVertex level ---
  data_manager: &data_manager
    deciders:
      write_always: {type: always}
      write_interval: &interval_continuous
        type: interval
        args:
          intervals:
            - [0, !expr 20 * 24, 24]
      write_at_equilibrium:
        type: equilibrium_decider
    triggers:
      build_interval: *interval_continuous
      build_once:
        type: once
        args:
          time: 0
      build_always: {type: always}
    tasks:
      Energy: &energy_continuous
        active: true
        trigger: build_once
        decider: write_always
      Energy_time: *energy_continuous

      Transitions: *energy_continuous
      Interface_length: *energy_continuous

      Vertices: &agents_continuous
        active: true
        trigger: build_interval
        decider: write_interval
      Cells: *agents_continuous
      Edges: *agents_continuous
      Cell_energies: &agent_energies_continuous
        active: false
        trigger: build_interval
        decider: write_interval
      Edge_energies: *agent_energies_continuous



# --- Data writing on model (Topology) level -----------------------------------
data_manager:
  deciders:
    write_always: {type: always}
    write_interval: &interval
      type: interval
      args:
        intervals:
          - [0, 10, 1]
          - [10, 1000, 5]
    write_logspace_interval: &logspace_interval
      type: interval
      args:
        intervals:
          - [0, 5, 1]
          - [5, 20, 5]
          - [20, 100, 10]
          - [100, 200, 50]
          - [200, 1000, 100]
          - [1000, 2000, 200]
          - [2000, 10000, 1000]
          - [10000, 20000, 2000]
          - [20000, 100000, 10000]
          - [100000, 200000, 20000]
  triggers:
    build_once:
      type: once
      args:
        time: 0
    build_always: {type: always}
    build_interval: *interval
    build_logspace_interval: *logspace_interval
  
  # these are the datasets written
  tasks:
    Energy: &energy
      active: true
      trigger: build_once
      decider: write_always
    Energy_time: *energy
    Continuous_time: *energy
    
    Transitions: *energy
    
    Vertices: &agents_equilibrium
      active: true
      trigger: build_interval
      decider: write_interval
    Cells: *agents_equilibrium
    Edges: *agents_equilibrium

    Cell_energies: &agent_energies
      active: true
      trigger: build_interval
      decider: write_interval
    Edge_energies: *agent_energies