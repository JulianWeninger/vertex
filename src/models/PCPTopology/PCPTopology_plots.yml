# The default plots of this model
#
# These can use the `based_on` feature with configurations specified in the
# accompanying *_base_plots.yml file and in the utopya base plot configs.
---
energy:
  based_on:
    - energy

energy_continuous:
  based_on: 
    - energy
    - select.uni.continuous_energy_terms

transitions:
  based_on: transitions

# effective diffusion
diffusion:
  based_on: diffusion_uni

# Displacement, i.e. MSD**0.5
displacement:
  based_on: displacement_uni


# Cell properties from cell data ----------------------------------------------
cell_properties:
  based_on:
    - .plot.multiplot
    - universe
    - select_cells_properties
  enabled: False

  select:
    property:
      path: data/PCPTopology/Cells

  to_plot: 
    - function: sns.lineplot
      data: !dag_result all_cells
      label: all cells
    - function: sns.lineplot
      data: !dag_result hair_cells
      label: hair cells
    - function: sns.lineplot
      data: !dag_result support_cells
      label: support cells

  x: time
  errorbar: sd

  helpers:
    set_labels:
      x: time

cell_properties/area:
  based_on: cell_properties
  select:
    property:
      transform:
        - .sel: [!dag_prev , {property: area}]
        - .rename: [!dag_prev , area]
  y: area
  helpers:
    set_labels:
      y: average area

cell_properties/area_preferential:
  based_on: cell_properties
  select:
    property:
      transform:
        - .sel: [!dag_prev , {property: area_preferential}]
        - .rename: [!dag_prev , area_preferential]
  y: area_preferential
  helpers:
    set_labels:
      y: average pref. area

cell_properties/shape:
  based_on: cell_properties
  select:
    property:
      transform:
          - .sel: [!dag_prev , {property: perimeter}]
          - .sel: [!dag_node -2, {property: area_preferential}]
          - pow: [!dag_node -1 , 0.5]
          - div: [!dag_node -3, !dag_node -1]
          - .rename: [!dag_prev , shape_index]
  y: shape_index
  helpers:
    set_labels:
      y: average shape index

# cell_properties/HC_neighbors:
#   based_on: cell_properties
#   select:
#     property:
#       transform:
#         - .sel: [!dag_prev , {property: num_hair_neighbors}]
#         - .rename: [!dag_prev , num_hair_neighbors]
#   y: num_hair_neighbors
#   helpers:
#     set_labels:
#       y: average hair neighbors

cell_properties/neighbors:
  based_on: cell_properties
  select:
    property:
      transform:
        - .sel: [!dag_prev , {property: num_neighbors}]
        - .rename: [!dag_prev , num_neighbors]
  y: num_neighbors
  helpers:
    set_labels:
      y: average neighbors

cell_properties/hexatic_order:
  based_on: cell_properties
  enabled: False
  select:
    property:
      transform:
        - .sel: [!dag_prev , {property: hexatic_order__corr}]
        - .rename: [!dag_prev , hexatic_order]
  y: hexatic_order
  to_plot: 
    - function: sns.lineplot
      data: !dag_result hair_cells_bulk
      label: hair cells
      errorbar: sd
  helpers:
    set_labels:
      y: average hexatic order

cell_properties/elongation:
  based_on: cell_properties
  select:
    property:
      transform:
        - .sel: [!dag_node -1, {property: q_x}]
        - .sel: [!dag_node -2, {property: q_y}]
        - call_lambda:
          - "lambda qx, qy: (qx**2 + qy**2)**0.5"
          - !dag_node -2
          - !dag_node -1
        - .rename: [!dag_prev , elongation]
  y: elongation
  to_plot: 
    - function: sns.lineplot
      data: !dag_result hair_cells_bulk
      label: hair cells
      errorbar: sd
  helpers:
    set_labels:
      y: average cell elongation

cell_properties_x_vs_y:
  enabled: false
  based_on:
    - .plot.multiplot
    - universe
    - select_cells_properties.xy_data

  select:
    x:
      path: data/PCPTopology/Cells
    y:
      path: data/PCPTopology/Cells
  hue: time
  
cell_properties/neighbors_vs_area:
  based_on: cell_properties_x_vs_y
  select:
    x:
      transform:
        - .sel: [!dag_prev , {property: area}]
        - .rename: [!dag_prev , area]
    y:
      transform:
        - .sel: [!dag_prev , {property: num_neighbors}]
        - .rename: [!dag_prev , num_neighbors]

  to_plot:
    - function: sns.scatterplot
      data: !dag_result hair_cells

  x: area
  y: num_neighbors

# cell_properties/HH_contacts_vs_area:
#   based_on: cell_properties_x_vs_y
#   enabled: False

#   select:
#     x:
#       transform:
#         - .sel: [!dag_prev , {property: area}]
#         - .rename: [!dag_prev , area]
#     y:
#       transform:
#         - .sel: [!dag_prev , {property: num_hair_neighbors}]
#         - .rename: [!dag_prev , num_hair_neighbors]

#   to_plot:
#     - function: sns.scatterplot
#       data: !dag_result hair_cells

#   x: area
#   y: num_hair_neighbors
#   hue: time
      
# energy_vs_SCHC_contacts:
#   based_on: universe
#   enabled: False

#   module: model_plots.PCPTopology
#   plot_func: scatter_xy_data

#   plot_time_color_gradient: True
#   cmap: plasma

#   select:
#     # the data containing all cells
#     _cells: data/PCPTopology/Cells
#     property:
#       path: data/PCPTopology/Cells
#       transform:
#       - .sel:
#         args: [!dag_tag _cells]
#         kwargs:
#           property: num_hair_neighbors
#     y: 
#       path: data/PCPTopology/Energy/Edge_contractility
    
#   transform:
#     - .sel:
#       args: [!dag_tag _cells]
#       kwargs:
#         property: cell_type
#     - create_mask:
#       args: [!dag_prev , ==, 2]
#     - .where:
#       args: [!dag_tag property, !dag_prev ]
#     - .mean:
#       args: [!dag_prev  ,id]
#       kwargs:
#         skipna: True
#       tag: x

#   helpers:
#     set_labels:
#       x: average HC neighbors for SCs
#       y: edge contractility energy

energy_vs_HCHC_contacts:
  based_on: energy_vs_SCHC_contacts
  enabled: False
  
  transform:
    - .sel:
      args: [!dag_tag _cells]
      kwargs:
        property: cell_type
    - create_mask:
      args: [!dag_prev , ==, 1]
    - .where:
      args: [!dag_tag property, !dag_prev ]
    - .mean:
      args: [!dag_prev , id]
      kwargs:
        skipna: True
      tag: x

  helpers:
    set_labels:
      x: average HC neighbors for HCs
      y: edge contractility energy
   
pressure:
  based_on:
    - .plot.multiplot
    - universe
    - select_cells_properties

  to_plot: 
    - function: sns.lineplot
      data: !dag_result all_cells
      label: all cells
      errorbar: sd
    - function: sns.lineplot
      data: !dag_result hair_cells
      label: hair cells
      errorbar: sd
    - function: sns.lineplot
      data: !dag_result support_cells
      label: support cells
      errorbar: sd

  select:
    property:
      path: data/PCPTopology/Cell_energies
      transform:
        - .sel: [!dag_prev , {term: area_elasticity__pressure}]
        - .rename: [!dag_prev , pressure]
        
  y: pressure
  x: time

tension:
  based_on:
    - .plot.multiplot
    - universe
    - select_edges_properties

  select:
    property:
      path: data/PCPTopology/Edge_energies
      transform:
        - .sel: [!dag_prev , {term: linetension_fluctuations__tension}]
        - .rename: [!dag_prev , tension]
    
  to_plot: 
    - function: sns.lineplot
      data: !dag_result all_edges
      label: all edges
      errorbar: sd
    - function: sns.lineplot
      data: !dag_result ss_edges
      label: support-support
      errorbar: sd
    - function: sns.lineplot
      data: !dag_result hs_edges
      label: support-hair
      errorbar: sd

  x: time
  y: tension


# --- Equilibrium cellular structures -----------------------------------------
equilibrium_cellular_structure:
  based_on:
    - cellular_structure

  property_interpolation_plot_kwargs:
    cmap: Reds


equilibrium_cellular_structure__area:
  based_on: equilibrium_cellular_structure
  enabled: false

  select:
    property:
      path: data/PCPTopology/Cells
      transform:
        - .sel: [!dag_prev , {property: area}]
        - .rename: [!dag_prev , Area]

equilibrium_cellular_structure__elongation:
  based_on: equilibrium_cellular_structure
  enabled: false
  select:
    vector_property:
      path: data/PCPTopology/Cells
      transform:
        - .sel: [!dag_prev , {property: [q_x, q_y]}]
        - .rename: [!dag_prev , Cell elongation]

  vector_property_is_nematic: True

  vector_property_kwargs:
    scale: 0.75
    headwidth: 1
    headlength: 1.

# equilibrium_cellular_structure__tension:
#   based_on: equilibrium_cellular_structure
#   enabled: false

#   select:
#     edge_property:
#       path: data/PCPTopology/Edge_energies
#       transform:
#         - .sel: [!dag_prev , {term: tension}]
#         - .rename: [!dag_prev , Tension]
#   quiver_kwargs:
#     cmap: cool
#     width: 0.01

# equilibrium_cellular_structure__pressure:
#   based_on: equilibrium_cellular_structure
#   enabled: false

#   select:
#     property:
#       path: data/PCPTopology/Cell_energies
#       transform:
#         - .sel: [!dag_prev , {property: pressure}]
#         - .rename: [!dag_prev , Pressure]
#   property_interpolation_plot_kwargs:
#     cmap: cool

equilibrium_cellular_structure__area_elasticity:
  based_on: equilibrium_cellular_structure
  enabled: false

  select:
    property:
      path: data/PCPTopology/Cell_energies
      transform:
        - .sel: [!dag_prev , {term: area_elasticity}]
        - .rename: [!dag_prev , Area elasticity]

equilibrium_cellular_structure__cell_contractility:
  based_on: equilibrium_cellular_structure
  enabled: false

  select:
    property:
      path: data/PCPTopology/Cell_energies
      transform:
        - .sel: [!dag_prev , {term: cell_contractility}]
        - .rename: [!dag_prev , Cell contractility]

equilibrium_cellular_structure__linetension:
  based_on: equilibrium_cellular_structure
  enabled: false

  select:
    edge_property:
      path: data/PCPTopology/Edge_energies
      transform:
        - .sel: [!dag_prev , {term: linetension_fluctuations}]
        - .rename: [!dag_prev , Line-tension fluct.]

# equilibrium_cellular_structure__edge_contractility:
#   based_on: equilibrium_cellular_structure
#   enabled: false

#   select:
#     edge_property:
#       path: data/PCPTopology/Edge_energies
#       transform:
#         - .sel: [!dag_prev , {term: edge_contractility}]
#         - .rename: [!dag_prev , Edge contractility]

# equilibrium_cellular_structure__hexatic_order:
#   based_on: equilibrium_cellular_structure
#   enabled: false

#   select:
#     property:
#       path: data/PCPTopology/Cells
#       transform:
#         - .sel: [!dag_prev , {property: hexatic_order__corr}]
#         - .sel: [!dag_node -2, {property: cell_type}]
#         - call_lambda:
#           - "lambda d, type: d.where(type==1)"
#           - !dag_node -2
#           - !dag_node -1
#         - .rename: [!dag_prev , Hexatic order]



# --- Continuous cellular structures ------------------------------------------
cellular_structure_continuous:
  based_on:
    - cellular_structure
  select:
    Vertices:
      path: data/PCPTopology/PCPVertex/Vertices
    Edges:
      path: data/PCPTopology/PCPVertex/Edges
    Cells:
      path: data/PCPTopology/PCPVertex/Cells
    Lx:
      path: data/PCPTopology/PCPVertex/Vertices
    Ly:
      path: data/PCPTopology/PCPVertex/Vertices
    skew_x:
      path: data/PCPTopology/PCPVertex/Vertices
    skew_y:
      path: data/PCPTopology/PCPVertex/Vertices
    curvature:
      path: data/PCPTopology/PCPVertex/Vertices


# --- Cellular geometries -----------------------------------------------------
cell_neighbourhood: !pspace
  based_on:
    - universe
    - _cell_neighbourhood

  select: &select_cell_neighbourhood
    area:
      path: data/PCPTopology/Cells
      transform:
        - .sel: [!dag_prev , {property: area}]
    cell_type:
      path: data/PCPTopology/Cells
      transform:
        - .sel: [!dag_prev , {property: cell_type}]
    shape_index:
      path: data/PCPTopology/Cells
      transform:
        - .sel: [!dag_prev , {property: perimeter}]
        - .sel: [!dag_node -2, {property: area_preferential}]
        - pow: [!dag_node -1 , 0.5]
        - div: [!dag_node -3, !dag_node -1]
        - .rename: [!dag_prev , shape_index]
    num_neighbors:
      path: data/PCPTopology/Cells
      transform:
        - .sel: [!dag_prev , {property: num_neighbors}]
    # num_hair_neighbors:
    #   path: data/PCPTopology/Cells
    #   transform:
    #     - .sel: [!dag_node -1, {property: cell_type}]
    #     - .sel: [!dag_node -2, {property: num_neighbors}]
    #     - .sel: [!dag_node -3, {property: num_neighbors__self}]
    #     - call_lambda:
    #       - "lambda type, nbs, _nbs: xr.where(type==0, _nbs, nbs - _nbs)"
    #       - !dag_node -3
    #       - !dag_node -2
    #       - !dag_node -1

  only_type: !sweep
    default: all
    values:
      - all
      # - hair
      # - support
      # - support_hair
      # - hair_hair
      
NotchDelta_densities:
  based_on: 
    - .plot.multiplot
    - universe

  enabled: False

  select:
    progenitor:
      path: data/PCPTopology/NotchDelta/Densities/Progenitor
      transform: &transform_NDdensity_to_dataframe
        - .rename: [!dag_prev , density]
        - import_and_call:
          args: [model_plots.PCPTopology.data_ops, to_dataframe, !dag_prev ]
    support:
      path: data/PCPTopology/NotchDelta/Densities/Support
      transform: *transform_NDdensity_to_dataframe
    hair:
      path: data/PCPTopology/NotchDelta/Densities/Hair
      transform: *transform_NDdensity_to_dataframe
    rosettes:
      path: data/PCPTopology/NotchDelta/Densities/Rosettes
      transform: *transform_NDdensity_to_dataframe

  x: time
  y: density

  to_plot:
  - function: sns.lineplot
    data: !dag_result progenitor
    label: progenitor
  - function: sns.lineplot
    data: !dag_result support
    label: support cells
  - function: sns.lineplot
    data: !dag_result hair
    label: hair cells
  - function: sns.lineplot
    data: !dag_result rosettes
    label: HC rosettes

  helpers:
    set_labels:
      x: time
      y: density
    set_limits:
      y: [0,1.]

    