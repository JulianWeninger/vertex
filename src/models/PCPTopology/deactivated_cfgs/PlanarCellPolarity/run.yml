# A minimal configuration to introduce heterogeneous edge contractility
# In quick and dirty during proliferation the energy is not minimized.
# This results in a significant (~ factor 100) speed up with similar results.
#
# The model can be run by typing : 
# $ cd /path-to-Utopia/vertex-model
# $ utopia run PCPTopology src/models/PCPTopology/cfgs/<case>/run.yml \
#          --plots-cfg src/models/PCPTopology/cfgs/<case>/eval.yml
#
# The content of this file is used to update the corresponding entries in
# ../PCPTopology_cfg.yml
---
perform_sweep: true
paths:
  model_note: default__PCP

worker_manager:
  nonzero_exit_handling: warn_all

parameter_space:
  num_steps: &NUM_STEPS 3
  times: &TIMES
    times:
      start: 1
      stop: 100000
      step: 1

  seed: !sweep
    default: 42
    range: [1]

  case: !sweep
    default: simple_shear
    values: 
      - simple_shear
      - oriented_cell_division
      - pure_shear
      # - bend

  PCPTopology:
    initialisation:
      1_by_proliferation:
        num_generations:  !coupled-sweep 
          target_name: case
          name: num_generations
          default: 1
          values: 
            - 1
            - 0
            - 1
            # - 1

    # The sequence of operations
    operations:      
      - iterate_pcp:
          times: []
          iterations_prolog: 1
          num_steps: 500
          minimization:
            mode: manual

      - !coupled-sweep
          target_name: case
          name: operation
          default:
            simple_shear: {                  
              times: *TIMES,
              shear_velocity: [0.1, 0.],
              deform_plastic: True,
              minimization: {mode: manual}
            }
          values:
            - {
                simple_shear: {                  
                  times: *TIMES,
                  shear_velocity: [0.1, 0.],
                  deform_plastic: True,
                  minimization: {mode: manual}
                }
              }
            - {
                proliferate: {
                  times: *TIMES,
                  num_increases: 0,
                  area_threshold: 0.,
                  angle_distribution: [0., 5.e-1],
                  minimization: {mode: manual}
                }
              }
            - {
                pure_shear: {                  
                  times: *TIMES,
                  shear_velocity: 0.05,
                  deform_plastic: True,
                  minimization: {mode: manual}
                }
              }
              
      - iterate_pcp: &iterate_pcp
          times: *TIMES
          num_steps: 1
          iterations_prolog: 250
          iterations: 250
          minimization:
            num_steps: 1
            mode: manual
            


    # --- Dynamics
    # Minimization parameter    
    PlanarCellPolarity:
      boundary_type: Neumann

      cell_cell_polarity_interaction: 0.1
      cell_polarity_exclusion: 0.05
      penalty_net_polarisation: 1.
      penalty_const_proteins: 10.
      use_lagrange_multipliers: false

      dt: 0.01
      proteins_per_edge: 3
      fluctuation: 1.e-2
      fluctuation_tau: 3.

      initialization: aligned
      alignment_orientation: 0.785
      alignment_orientation_stddev: 0.2

      log_level: info
      
      data_manager:
        deciders:
          write_always:
            type: always
          write_interval:
            type: interval
            args: 
              intervals:
                - [0, 10000000000000, 5]
        triggers:
          build_once: {type: once, args: {time: 0}}
        tasks:
          Energy: &ENERGY_PCP
            active: true
            decider: write_interval
            trigger: build_once
          Energy_time: *ENERGY_PCP
          
    PCPVertex:
      agent_manager:
        cell_manager:
          agent_params:
            polarity: -1.5708   # -Pi / 2
        setup_params:
          hexagonal:
            lattice_rows: !coupled-sweep
              target_name: case
              default: 4
              values: 
                - 4
                - 8
                - 16
                # - 4
            lattice_columns: !coupled-sweep
              target_name: case
              default: 4
              values: 
                - 16
                - 8
                - 4
                # - 16


      # continuous data manager
      data_manager:
        deciders:
          write_energy:
            type: interval
            args: 
              intervals:
                - [0, 1000000000, 50]
        tasks:
          Energy_total: &energy_continuous {decider: write_energy}
          Energy_linetension: *energy_continuous
          Energy_areaelasticity: *energy_continuous
          Energy_cell_contractility: *energy_continuous
          Energy_edge_contractility: *energy_continuous
          Energy_boundary_area_elasticity: {active: false}
          Energy_boundary_shape_elasticity: {active: false}
          Energy_time: *energy_continuous
          transitions: *energy_continuous

          Vertices: &position {active: false}
          Cells: *position
          Edges: *position

    # equilibrium data manager
    data_manager:
      deciders:
        write_always:
          type: always

        write_interval: &interval
          type: interval
          args:
            intervals:
              - [0, 5]
              - [5, 50, 5]
              - [50, 1000, 10]
              # - [1000, 10001, 200]
              # - [10000, 100001, 5000]
      triggers:
        build_interval: *interval
      tasks:
        PCP_Cells: {active: true}
        PCP_Proteins: {active: true}
        
        Energy_boundary_area_elasticity: {active: false}
        Energy_boundary_shape_elasticity: {active: false}
