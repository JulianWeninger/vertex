### What does this MR do?
<!-- State important details of your model implementation here -->
This MR ... _fill this in_


### Is there something that needs to be double checked?
<!-- Is there something a reviewer should look out for _especially_? -->
_fill this in_


### Can this MR be accepted?
- [ ] Implemented / improved the model
- [ ] Added or adapted model tests
   - [ ] Python tests
   - [ ] ~~C++ tests~~ __(optional)__
   - [ ] Added model to `test:models` job in [`.gitlab-ci.yml`](.gitlab-ci.yml)
- [ ] Implemented plotting functions
- [ ] Added documentation
   - [ ] Model itself is reasonably well documented
   - [ ] Model description in [`doc/models/`](doc/models/) (`.rst` format!)
- [ ] Model requirements (see documentation) are all fulfilled
- [ ] Reasonably up-to-date with current master
- [ ] Pipeline passing without warnings
- [ ] Squash option set <!-- unless there's a good reason not to squash -->
- [ ] Approved by @ ... <!-- Add reviewer(s) here once no longer WIP -->


### Related issues
Closes #
<!-- For automatic closing, you can add commas between issue numbers-->
